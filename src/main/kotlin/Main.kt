import java.io.FileInputStream
import java.util.*

fun main() {
    var sc = Scanner(FileInputStream("roles.txt"))
    val roles = arrayListOf<String>()
    while (sc.hasNextLine())
        roles.add(sc.nextLine())

    sc.close()

    sc = Scanner(FileInputStream("textLines.txt"))
    val textLines = arrayListOf<String>()
    while (sc.hasNextLine())
        textLines.add(sc.nextLine())

    val paper = mutableMapOf<String, String>()

    roles.forEach{
        paper[it] = "$it:\n"
    }

    var num = 0
    textLines.forEach{
        num++
        val separator = it.indexOf(":")

        val name = it.substring(0, separator)
        val text = it.substring(separator + 2)

        paper[name] += "$num) $text\n"
    }

    paper.forEach{
        println(it.value)
    }
}